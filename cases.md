[Работа с файлами](#1)

[Сортировка средствами python](#2)

[Quick sort](#3)

[Бинарный поиск](#4)

[Фибоначчи](#5)


### <a name="1"></a>Работа с файлами
Открытие
```python
with open('data.txt', 'r') as f:
    pass
```
Полное чтение
```python
content = f.read()
```
Список строк
```python
lines = f.readlines()
```
Запись
```python
with open('data.txt', 'w') as f:
    f.write('asd\ndfg')
```

### <a name="2"></a>Сортировка средствами python
```python
from operator import itemgetter


if __name__ == '__main__':
    data = [{'a': 4}, {'a': 2}, {'a': 3}]
    result = sorted(data, key=itemgetter('a'), reverse=True)
```

### <a name="3"></a>Quick sort
```python
def quick_sort(array):
    less = []
    equals = []
    greater = []

    if len(array) > 1:
        pivot = array[0]
        for x in array:
            if x < pivot:
                less.append(x)
            elif x == pivot:
                equals.append(x)
            else:
                greater.append(x)
        result = quick_sort(less) + equals + quick_sort(greater)
    else:
        result = array

    return result
```

### <a name="4"></a>Бинарный поиск
```python
from random import randint

a = []
for i in range(15):
    a.append(randint(1, 50))
a.sort()
print(a)

# искомое число
value = int(input())

mid = len(a) // 2
low = 0
high = len(a) - 1

while a[mid] != value and low <= high:
    if value > a[mid]:
        low = mid + 1
    else:
        high = mid - 1
    mid = (low + high) // 2

if low > high:
    print("No value")
else:
    print("ID =", mid)
```

### <a name="5"></a>Фибоначчи
```python
def fibonacci():
    prev, cur = 0, 1
    while True:
        yield prev
        prev, cur = cur, prev + cur


for i in fibonacci():
    print(i)
    if i > 100:
        break
```
