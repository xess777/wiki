# Вопросы
[1. Написать запрос, который удаляет дубликаты из таблицы на чистом SQL](#1)


### <a name="1"></a>1. Написать запрос, который удаляет дубликаты из таблицы на чистом SQL

Таблица **basket**:
| id | fruit  |
|:--:|--------|
| 1  | apple  |
| 2  | apple  |
| 3  | orange |
| 4  | orange |
| 5  | orange |
| 6  | banana |

**Поиск дубликатов**
```sql
SELECT
    fruit,
    COUNT( fruit )
FROM
    basket
GROUP BY
    fruit
HAVING
    COUNT( fruit )> 1
ORDER BY
    fruit;
```

**Удаление дубликатов**
```sql
DELETE
FROM
    basket a
        USING basket b
WHERE
    a.id < b.id
    AND a.fruit = b.fruit;
```

**USING** — это сокращённая запись условия, полезная в ситуации, когда с обеих сторон соединения столбцы имеют одинаковые имена. Она принимает список общих имён столбцов через запятую и формирует условие соединения с равенством этих столбцов. Например, запись соединения T1 и T2 с USING (a, b) формирует условие ON T1.a = T2.a AND T1.b = T2.b.


**Удаление с помощью оконных функций**

This query does that for all rows of tablename having the same col1, col2, and col3.
Sometimes a timestamp field is used instead of an ID field.
```sql
DELETE FROM tablename
WHERE id IN (
    SELECT id FROM (
        SELECT
            id,
            ROW_NUMBER() OVER (partition BY col1, col2, col3 ORDER BY id) AS rnum
        FROM tablename
    ) t
    WHERE t.rnum > 1);
```
