# Вопросы
[1. Что такое метаклассы и для чего они нужны?](#1)

[2. Что такое «Сборщик мусора» и как он работает?](#2)

[3. Что такое менеджер контекста?](#3)

[4. Что такое итераторы?](#4)

[5. Что такое генераторы?](#5)

[6. Что такое дескриптор?](#6)

[7. Как работает try...except...else...finally?](#7)

[8. Как работает else в for и while?](#8)

[9. Сложность in в dict, tuple, set, list в О-большое нотации](#9)

[10. Что такое декоратор?](#10)

[11. Инкапсуляция в python, _ и __](#11)

[12. Этапы создания объекта](#12)

[13. Что такое __slots__](#13)

[14. кросс-импорты](#14)


### <a name="1"></a>1. Что такое метаклассы и для чего они нужны?

### <a name="2"></a>2. Что такое «Сборщик мусора» и как он работает?

### <a name="3"></a>3. Что такое менеджер контекста?

### <a name="4"></a>4. Что такое итераторы?
`Итератор` — интерфейс, предоставляющий доступ к элементам коллекции и навигацию по ним.

`Внешний итератор` — это классический (pull-based) итератор, когда процессом обхода явно управляет клиент путем вызова метода Next.

`Внутренний итератор` — это push-based-итератор, которому передается callback функция, и он сам уведомляет клиента о получении следующего элемента.

Итерируемый объект это любой объект который реализует метод `__iter__`, либо `__getitem__`, если используется функция iter().

**итератор в python** — это любой объект, реализующий метод `__next__` без аргументов, который должен вернуть следующий элемент или ошибку StopIteration.

Примеры:
```python
class ListIterator(collections.abc.Iterator):
    def __init__(self, collection, cursor):
        self._collection = collection
        self._cursor = cursor

    def __next__(self):
        if self._cursor + 1 >= len(self._collection):
            raise StopIteration()
        self._cursor += 1
        return self._collection[self._cursor]

class ListCollection(collections.abc.Iterable):
    def __init__(self, collection):
        self._collection = collection

    def __iter__(self):
        return ListIterator(self._collection, -1)
```

Варианты работы:
```python
collection = [1, 2, 5, 6, 8]
aggregate = ListCollection(collection)

for item in aggregate:
    print(item)

print("*" * 50)

itr = iter(aggregate)
while True:
    try:
        print(next(itr))
    except StopIteration:
        break
```

### <a name="5"></a>5. Что такое генераторы?
Генератор - функция, возвращающая подвид итератора, генерирующий значения, с помощью ключевого слова `yield`.

Любая функция в Python, в теле которой встречается ключевое слово yield, называется `генераторной функцией` — при вызове она возвращает объект-генератор.

`Объект-генератор` реализует интерфейс итератора, соответственно с этим объектом можно работать, как с любым другим итерируемым объектом.


Пример:
```python
def fibonacci():
    prev, cur = 0, 1
    while True:
        yield prev
        prev, cur = cur, prev + cur

for i in fibonacci():
    print(i)
    if i > 100:
        break
```

### <a name="6"></a>6. Что такое дескриптор?

### <a name="7"></a>7. Как работает try...except...else...finally?

### <a name="8"></a>8. Как работает else в for и while?

### <a name="9"></a>9. Сложность in в dict, tuple, set, list в О-большое нотации

### <a name="10"></a>10. Что такое декоратор?

### <a name="10"></a>11. Инкапсуляция в python, _ и __

### <a name="10"></a>12. Этапы создания объекта

### <a name="10"></a>13. Что такое `__slots__`

### <a name="10"></a>14. кросс-импорты
[Circular Dependency](https://stackabuse.com/python-circular-imports/)
