import abc
from typing import Union, List


Num = Union[int, float]


class StrategySort(metaclass=abc.ABCMeta):
    """Описывает интерфейс стратегий (алгоритмов)."""

    @abc.abstractmethod
    def sort(self, data: List[Num]) -> None:
        pass


class BubbleSort(StrategySort):
    """Реализует алгоритм сортировки пузырьком."""

    def sort(self, data: List[Num]) -> None:
        size = len(data)

        if size < 2:
            return

        n = 1
        while n < size:
            for i in range(size - n):
                if data[i] > data[i + 1]:
                    data[i], data[i + 1] = data[i + 1], data[i]
            n += 1


class MinSort(StrategySort):
    """Реализует алгоритм сортировки выбором."""

    def sort(self, data: List[Num]) -> None:
        size = len(data)

        if size < 2:
            return

        k = 0
        while k < size - 1:
            m = k
            i = k + 1
            while i < size:
                if data[i] < data[m]:
                    m = i
                i += 1
            t = data[k]
            data[k] = data[m]
            data[m] = t
            k += 1


class Context:
    """Реализует контекст выполнения той или иной стратегии."""
    def __init__(self) -> None:
        self._strategy = None

    def set_algorithm(self, strategy: 'StrategySort') -> None:
        """Подмена стратегии (алгоритма)."""
        self._strategy = strategy

    def sort(self, data: List[Num]) -> None:
        """Сортировка в зависимости от выбраной стратегии (алгоритма)."""
        assert self._strategy is not None

        self._strategy.sort(data)
