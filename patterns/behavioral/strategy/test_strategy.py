import unittest

from patterns.behavioral.strategy.strategy import Context, BubbleSort, MinSort


class TestStrategy(unittest.TestCase):

    def test_strategy(self):
        data1 = [8, 2, 6, 7, 1, 3, 9, 5, 4]
        data2 = [8, 2, 6, 7, 1, 3, 9, 5, 4]

        ctx = Context()
        ctx.set_algorithm(BubbleSort())
        ctx.sort(data1)
        ctx.set_algorithm(MinSort())
        ctx.sort(data2)

        expect = [1, 2, 3, 4, 5, 6, 7, 8, 9]

        self.assertEqual(data1, expect)
        self.assertEqual(data2, expect)


if __name__ == 'main':
    unittest.main()
