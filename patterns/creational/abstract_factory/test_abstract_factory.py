import unittest

from patterns.creational.abstract_factory.abstract_factory import (
    CocaColaFactory)


class TestAbstractFactory(unittest.TestCase):

    def test_abstract_factory(self):
        volume = 1.5
        factory = CocaColaFactory()
        drink = factory.create_drink(volume)
        bottle = factory.create_bottle(volume)
        bottle.interact_drink(drink)

        self.assertEqual(drink.get_volume(), bottle.get_drink_volume())


if __name__ == 'main':
    unittest.main()
