import abc


class AbstractDrink(metaclass=abc.ABCMeta):
    """Описывает интерфейс напитка."""

    @abc.abstractmethod
    def get_volume(self) -> float:
        """Возвращает объем напитка."""
        pass


class AbstractBottle(metaclass=abc.ABCMeta):
    """Описывает интерфейс бутылки."""

    @abc.abstractmethod
    def interact_drink(self, drink: 'AbstractDrink') -> None:
        """Взаимодействие бутылки с напитком."""
        pass

    @abc.abstractmethod
    def get_bottle_volume(self) -> float:
        """Возвращает объем бутылки."""
        pass

    @abc.abstractmethod
    def get_drink_volume(self) -> float:
        """Возвращает объем напитка."""
        pass


class AbstractFactory(metaclass=abc.ABCMeta):
    """Описывает интерфейс Абстрактной Фабрики по производству газировки.
    Конкретные фабрики должны его реализовать.
    """

    @abc.abstractmethod
    def create_drink(self, volume: float) -> 'AbstractDrink':
        """Создает напиток."""
        pass

    @abc.abstractmethod
    def create_bottle(self, volume: float) -> 'AbstractBottle':
        """Создает бутылку."""
        pass


class CocaColaDrink(AbstractDrink):

    def __init__(self, volume: float) -> None:
        self._volume = volume

    def get_volume(self) -> float:
        """Возвращает объем напитка."""
        return self._volume


class CocaColaBottle(AbstractBottle):

    def __init__(self, volume: float) -> None:
        self._volume = volume
        self._drink = None

    def interact_drink(self, drink: 'CocaColaDrink') -> None:
        """Взаимодействие бутылки с напитком."""
        self._drink = drink

    def get_bottle_volume(self) -> float:
        """Возвращает объем бутылки."""
        return self._volume

    def get_drink_volume(self) -> float:
        """Возвращает объем напитка."""
        assert self._drink is not None

        return self._drink.get_volume()


class CocaColaFactory(AbstractFactory):

    def create_drink(self, volume: float) -> 'CocaColaDrink':
        """Создает напиток."""
        return CocaColaDrink(volume)

    def create_bottle(self, volume: float) -> 'CocaColaBottle':
        """Создает бутылку."""
        return CocaColaBottle(volume)
