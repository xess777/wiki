
## Паттерны проектирования с примерами на Python

Это коллекция из 22 популярных паттернов проектирования с примерами кода на языке Python и кратким описанием паттерна.

В кратких описаниях будут употребляться классические термины, такие как Класс, Объект, Абстрактный Класс.

## Паттерны

### [Порождающие (Creational)](creational)

* [Абстрактная фабрика (Abstract Factory)](creational/abstract_factory)
* [Строитель (Builder)](creational/builder)
* [Фабричный метод (Factory Method)](creational/factory_method)
* [Прототип (Prototype)](creational/prototype)
* [Одиночка (Singleton)](creational/singleton)

### [Стуктурные (Structural)](structural)

* [Адаптер (Adapter)](structural/adapter)
* [Мост (Bridge)](structural/bridge)
* [Компоновщик (Composite)](structural/composite)
* [Декоратор (Decorator)](structural/decorator)
* [Фасад (Facade)](structural/facade)
* [Приспособленец (Flyweight)](structural/flyweight)
* [Заместитель (Proxy)](structural/proxy)

### [Поведенческие (Behavioral)](behavioral)

* [Цепочка ответственности (Chain Of Responsibility)](behavioral/chain_of_responsibility)
* [Команда (Command)](behavioral/command)
* [Итератор (Iterator)](behavioral/iterator)
* [Посредник (Mediator)](behavioral/mediator)
* [Хранитель (Memento)](behavioral/memento)
* [Наблюдатель (Observer)](behavioral/observer)
* [Состояние (State)](behavioral/state)
* [Стратегия (Strategy)](behavioral/strategy)
* [Шаблонный метод (Template Method)](behavioral/template_method)
* [Посетитель (Visitor)](behavioral/visitor)
