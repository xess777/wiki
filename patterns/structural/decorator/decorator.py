import abc


class Component(metaclass=abc.ABCMeta):
    """Описывает интерфейс для компонента."""

    @abc.abstractmethod
    def operation(self):
        pass


class Decorator(metaclass=abc.ABCMeta):
    """Описывает интерфейс для декоратора."""

    def __init__(self, component):
        self._component = component

    @abc.abstractmethod
    def operation(self):
        pass


class ConcreteComponent(Component):
    """Реализует компонент."""

    def operation(self):
        return 'I am component'


class ConcreteDecorator(Decorator):
    """Реализует декоратор."""

    def operation(self):
        return '<strong>' + self._component.operation() + '</strong>'
