import unittest

from patterns.structural.decorator.decorator import (
    ConcreteComponent, ConcreteDecorator)


class TestDecorator(unittest.TestCase):

    def test_decorator(self):
        component = ConcreteComponent()
        decorator = ConcreteDecorator(component)
        expect = '<strong>I am component</strong>'
        self.assertEqual(decorator.operation(), expect)


if __name__ == 'main':
    unittest.main()
