# Термины

**Инвариа́нт** — это свойство некоторого класса (множества) математических объектов, остающееся неизменным при преобразованиях определённого типа.

**Инвариант класса** (или **инвариант типа**) — инвариант, используемый для ограничения объектов класса. Методы класса должны сохранять инвариант.

Во время создания классов устанавливаются их инварианты, которые постоянно поддерживаются между вызовами публичных методов. Временное нарушение классовой инвариантности между частными вызовами метода возможно, хотя и нежелательно.

Инвариант объекта представляет собой конструкцию программирования, состоящую из набора инвариантных свойств. Это гарантирует, что объект всегда будет соответствовать предопределенным условиям, и поэтому методы могут всегда ссылаться на объект без риска сделать неточные презумпции. Определение инвариантов классов может помочь программистам и тестировщикам обнаружить больше ошибки при тестировании программного обеспечения.